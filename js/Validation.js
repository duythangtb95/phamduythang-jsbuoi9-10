// kiểm tra số tài khoản
const tbTKNV = "tbTKNV"
function checkTKNV(tknv, dsnv) {
    var index = timKiemViTri(tknv, dsnv);
    var reg = /^\d{4,6}$/;
    let isTknv = reg.test(tknv);
    if (index !== -1) {
        showMessageErr(tbTKNV, "Tài khoản đã tồn tại");
        return false;
    } else if (isTknv == false) {
        showMessageErr(tbTKNV, "Tài khoản cần chứa 4 tới 6 chữ số");
        return false;
    } else if (tknv == "") {
        showMessageErr(tbTKNV, "Tà khoản không được bỏ trống")
    }
    else {
        showMessageErr(tbTKNV, "");
        return true;
    }
}

// kiểm tra tên
const tbName = "tbName";
function checkTen(name) {
    var reg = /^[a-zA-Z ]*$/;
    let isName = reg.test(name);
    if (name == "") {
        showMessageErr(tbName, "Tên nhân viên không được bỏ trống");
        return false
    } else if (isName == false) {
        showMessageErr(tbName, "Tên nhân viên phải là chữ");
        return false
    } else {
        showMessageErr(tbName, "");
        return true
    }
}

// kiểm tra email
const tbEmail = "tbEmail";
function checkEmail(email) {
    var reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
    var isEmail = reg.test(email);
    if (email == "") {
        showMessageErr(tbEmail, "Email không được bỏ trống");
        return false;
    } else if (isEmail == false) {
        showMessageErr(tbEmail, "Email không hợp lệ");
        return false;
    } else {
        showMessageErr(tbEmail, "");
        return true;
    }

}

// kiểm tra password
const tbMatKhau = "tbPassword";
function checkPassword(password) {
    var reg = /(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,10})/;
    var isPassword = reg.test(password);
    if (password == "") {
        showMessageErr(tbMatKhau, "Mật khẩu không được bỏ trống");
        return false;
    } if (isPassword == false) {
        showMessageErr(tbMatKhau, "Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt");
        return false;
    } else {
        showMessageErr(tbMatKhau, "");
        return true;
    }
}

// kiểm tra ngày làm
const tbDatepicker = "tbDatepicker";
function checkDatepicker(datepicker) {
    var reg = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
    var isDatepicker = reg.test(datepicker);
    if (datepicker == "") {
        showMessageErr(tbDatepicker, "Ngày không được bỏ trống");
        return false;
    } if (isDatepicker == false) {
        showMessageErr(tbDatepicker, "Ngày định dạng mm/dd/yyyy");
        return false;
    } else {
        showMessageErr(tbDatepicker, "");
        return true;
    }

}

// kiểm tra lương cb
const tbLuongCB = "tbLuongCB";
function checkLuongCB(luongCB) {
    if (luongCB == 0) {
        showMessageErr(tbLuongCB, "Lương cơ bản không được bỏ trống");
        return false;
    } else if (luongCB > 20000000 || luongCB < 1000000) {
        showMessageErr(tbLuongCB, "Lương cơ bản từ 1000000 - 20000000");
        return false;
    } else {
        showMessageErr(tbLuongCB, "");
        return true;
    }
}

// kiểm tra chức vụ 
const tbChucVu = "tbChucVu";
function checkChucvu(chucvu) {
    if (chucvu == "Chọn chức vụ"){
        showMessageErr(tbChucVu, "Chưa chọn chức vụ");
        return false;
    } else{
        showMessageErr(tbChucVu, "");
        return true;
    }

}

// kiểm tra số giờ làm
const tbGiolam = "tbGiolam";
function checkGioLam(gioLam) {
    if (gioLam == 0) {
        showMessageErr(tbGiolam, "Giờ làm không được bỏ trống");
        return false;
    } else if (gioLam > 200 || gioLam < 80) {
        showMessageErr(tbGiolam, "Số giờ làm trong tháng 80 - 200 giờ");
        return false;
    } else {
        showMessageErr(tbGiolam, "");
        return true;
    }
}




