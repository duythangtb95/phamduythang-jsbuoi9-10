// Functin 1: In ra table danh sách nhân viên
function ThemNV() {
    var nv = layThongTinNV()
    var isValid = true;
    // kiểm tra lần lượt các giá trị
    isValid = checkTKNV(nv.tknv, dsnv);
    isValid = isValid & checkTen(nv.name);
    isValid = isValid & checkEmail(nv.email);
    isValid = isValid & checkPassword(nv.password);
    isValid = isValid & checkDatepicker(nv.datepicker);
    isValid = isValid & checkLuongCB(nv.luongCB);
    isValid = isValid & checkChucvu(nv.chucvu);
    isValid = isValid & checkGioLam(nv.gioLam);

    if (isValid) {
        dsnv.push(nv);
        luuLocalStorage(dsnv);
        renderDsnv(dsnv);
        alert("Đã thêm thành công");
    }
}

// Function 2:Xoá nhân viên
function XoaNV(tknv) {
    var vitri = timKiemViTri(tknv, dsnv)
    if (vitri !== -1) {
        dsnv.splice(vitri, 1);
        luuLocalStorage(dsnv);
        renderDsnv(dsnv);
    }
}

// Function 3:Cập nhật nhân viên (có validation)

// 1.User bấm vào nút sửa để xác định dòng cần sử từ đó show hiện lên cửa sổ cần sửa và thông tin điều sẵn của ô cần sửa
function SuaNV(tknv) {
    var vitri = timKiemViTri(tknv, dsnv)
    if (vitri !== -1) {
        showThongTinLenForm(dsnv[vitri]);
    }
    document.getElementById("tknv").disabled = true;
}

// 2. cho user sửa trên trang điền sau đó bấm vào ô cập nhật để cập nhật thông tin
function CapNhat() {
    var nv = layThongTinNV();
    var isValid = true;
    // kiểm tra lần lượt các giá trị
    // isValid = isValid & checkTKNV(nv.tknv, dsnv);
    isValid = isValid & checkTen(nv.name);
    isValid = isValid & checkEmail(nv.email);
    isValid = isValid & checkPassword(nv.password);
    isValid = isValid & checkDatepicker(nv.datepicker);
    isValid = isValid & checkLuongCB(nv.luongCB);
    isValid = isValid & checkChucvu(nv.chucvu);
    isValid = isValid & checkGioLam(nv.gioLam);

    if (isValid) {
        var index = timKiemViTri(nv.tknv, dsnv);
        dsnv[index] = nv;
        luuLocalStorage(dsnv);
        renderDsnv(dsnv);
        alert("Đã cập nhật thành công");
    }
}


// Function 4:Tìm Nhân Viên theo loại (xuất săc, giỏi, khá...) và hiển thị

function TimkiemNV() {
    var loainv = document.getElementById("searchXeploai").value;
    var dsnvfilter = [];
    // từ xếp loại cần tìm, ta lọc ra các nhân viên truyền vào dsnvfilter rồi in ra màn hình
    for (index = 0; index < dsnv.length; index++) {
        var item = dsnv[index];
        console.log(item.gioLam);
        console.log(xeploai(item.gioLam));
        if (loainv == xeploai(item.gioLam)) {
            dsnvfilter.push(dsnv[index])
        }
    }
    // nếu chọn tất cả thì danh sách bằng danh sách ban đầu
    if (loainv == "Tất cả") {
        dsnvfilter = dsnv;
    }

    renderDsnv(dsnvfilter);

    // tạo hàm xếp loại
    function xeploai(gioLam) {
        if (gioLam < 160) {
            return "Trung bình";
        } else if (gioLam < 176) {
            return "Khá";
        } else if (gioLam < 192) {
            return "Giỏi";
        } else {
            return "Xuất sắc";
        }
    }

}
