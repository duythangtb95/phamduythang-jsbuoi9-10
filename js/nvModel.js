// tạo function object NhanVien
const Sep = "Sếp";
const Truongphong = "Trưởng phòng";
const Nhanvien = "Nhân viên";
function NhanVien(tknv, name, email, password, datepicker, luongCB, chucvu, gioLam) {
    this.tknv = tknv;
    this.name = name;
    this.email = email;
    this.password = password;
    this.datepicker = datepicker;
    this.luongCB = luongCB;
    this.chucvu = chucvu;
    this.gioLam = gioLam;
    this.tinhTongluong = function () {
        if (this.chucvu == Sep) {
            return this.luongCB * 3;
        } else if (this.chucvu == Truongphong) {
            return this.luongCB * 2;
        } else if (this.chucvu == Nhanvien) {
            return this.luongCB;
        } else {
            return null;
        }
    }
    this.xeploai = function () {
        if (this.gioLam < 160) {
            return "Trung bình"
        } else if (this.gioLam < 176) {
            return "Khá"
        } else if (this.gioLam < 192) {
            return "Giỏi"
        } else {
            return "Xuất sắc"
        }
    }
}

// tạo array dsnv
var dsnv = [];

