// function lấy thông tin từ Form
function layThongTinNV() {

  // lấy dữ liệu từ form
  var tknv = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var datepicker = document.getElementById("datepicker").value;
  var luongCB = Number(document.getElementById("luongCB").value);
  var chucvu = document.getElementById("chucvu").value;
  var gioLam = Number(document.getElementById("gioLam").value);
  // tạo nv
  var nv = new NhanVien(tknv, name, email, password, datepicker, luongCB, chucvu, gioLam);
  return nv;
}

// function in thông tin ra vào danh sách Nv
function renderDsnv(nvArr) {
  var contentHTML = "";
  for (var index = 0; index < nvArr.length; index++) {
    var currentNv = nvArr[index];

    var contentTr =
      `<tr>
         <td>${currentNv.tknv}</td>
         <td>${currentNv.name}</td>
         <td>${currentNv.email}</td>
         <td>${currentNv.datepicker}</td>
         <td>${currentNv.chucvu}</td>
         <td>${currentNv.tinhTongluong()}</td>
         <td>${currentNv.xeploai()}</td>
         <td><button onclick="XoaNV(${currentNv.tknv});">Xoá</button></td>
         <td><button onclick="SuaNV(${currentNv.tknv});"  data-toggle="modal"
         data-target="#myModal">Cập nhật</button></td>
      </tr>`
      ;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// lưu dữ liệu localstorage
const DSNV = "DSNV";
function luuLocalStorage(dsnv) {
  let jsonDsnv = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, jsonDsnv);
}

// lấy dữ liệu từ localstorage
var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  var nvArr = JSON.parse(dataJson)
  for (index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new NhanVien(
      item.tknv,
      item.name,
      item.email,
      item.password,
      item.datepicker,
      item.luongCB,
      item.chucvu,
      item.gioLam,
    );
    dsnv.push(nv);
  }
  renderDsnv(dsnv);
}

// Hiện cảnh báo nhập dữ liệu
function showMessageErr(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
  if (message == "") {
    document.getElementById(idErr).style.display = "none";
  } else {
    document.getElementById(idErr).style.display = "inline";
  }
}

// tìm kiếm vị trí
function timKiemViTri(id, nvArr) {
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    if (item.tknv == id) {
      return index;
    }
  }
  return -1;
}

// show thông tin lên Form
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.tknv ;
  document.getElementById("name").value = nv.name ;
  document.getElementById("email").value = nv.email ;
  document.getElementById("password").value = nv.password ;
  document.getElementById("datepicker").value = nv.datepicker ;
  document.getElementById("luongCB").value = nv.luongCB ;
  document.getElementById("chucvu").value = nv.chucvu ;
  document.getElementById("gioLam").value = nv.gioLam ;
}